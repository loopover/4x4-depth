
#[derive(Clone, PartialEq, Eq, Hash)]
pub struct Board {
    rows: [u16; 4]
}

impl Board {
    pub fn new() -> Board {
        Board { rows: [0x3210, 0x7654, 0xba98, 0xfedc] }
    }

    pub fn move_row(&mut self, i: usize, n: isize) {
        let row = &mut self.rows[i];
        let indices = [*row % 16, (*row >> 4) % 16, (*row >> 8) % 16, *row >> 12];
        *row = 0; for col in 0..4 {
            *row += indices[(col - n + 4) as usize % 4] << (col * 4)
        }
    }

    pub fn move_col(&mut self, i: usize, n: isize) {
        let mut indices = [0; 4];
        for j in 0..4 { indices[j] = (self.rows[j] >> 4 * i) % 16 }
        for row in 0..4 {
            self.rows[row] = self.rows[row] & !(0xf << (i * 4)) | (indices[(row as isize - n + 4) as usize % 4]) << (i * 4);
        }
    }
}

impl std::fmt::Debug for Board {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        for i in 0..4 {
            let x = self.rows[i];
            writeln!(f, "{:2} {:2} {:2} {:2}", x % 16, (x >> 4) % 16, (x >> 8) % 16, x >> 12)?;
        }
        Ok(())
    }
}
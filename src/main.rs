#![feature(duration_float)]

mod board;
use self::board::Board;
use std::collections::HashSet;
use std::time::Instant;

fn main() {
    let mut sets: Vec<HashSet<Board>> = Vec::new();
    let start_time = Instant::now();
    loop {
        let mut set = HashSet::new();
        if sets.len() == 0 {
            set.extend(get_next_possible_states(&Board::new()));
        } else {
            let last_set = sets.last().unwrap();

            for (i, board) in last_set.iter().enumerate() {
                if i % 250000 == 250000 - 1 { println!("{} %", (i * 1000 / last_set.len()) as f32 / 10.0) }
                
                let mut next_boards = Vec::new();
                for new_board in &get_next_possible_states(&board) {
                    if new_board == board { continue }

                    let mut exists_in_prev_depth = false; 
                    for set in &sets { if set.contains(new_board) {
                        exists_in_prev_depth = true;
                        break
                    } }
                    if !exists_in_prev_depth {
                        next_boards.push(new_board.clone());
                    }
                }
                set.extend(next_boards);
            }
        }
        println!("depth {}, {} perms", sets.len() + 1, set.len());
        if set.len() == 0 { break }
        sets.push(set);
        if sets.len() == 6 { break }
    }

    let elapsed = start_time.elapsed();
    println!("The board has a depth of {}", sets.len());
    println!("Took {:.3} seconds", elapsed.as_float_secs());
}

fn get_next_possible_states(board: &Board) -> Vec<Board> {
    let mut boards = Vec::with_capacity(2 * 4 * 3);
    for index in 0..4 {
        for n in 1..4 {
            let mut board = board.clone();
            board.move_col(index, n);
            boards.push(board);
        }
    }
    for index in 0..4 {
        for n in 1..4 {
            let mut board = board.clone();
            board.move_row(index, n);
            boards.push(board);
        }
    }
    boards
}